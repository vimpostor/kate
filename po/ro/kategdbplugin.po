# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Sergiu Bivol <sergiu@cip.md>, 2011.
# Cristian Oneț <onet.cristian@gmail.com>, 2011, 2012.
# Cristian Buzduga <cristianbzdg@gmail.com>, 2011.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-03-17 01:00+0000\n"
"PO-Revision-Date: 2012-01-21 12:41+0200\n"
"Last-Translator: Cristian Oneț <onet.cristian@gmail.com>\n"
"Language-Team: Romanian <kde-i18n-ro@kde.org>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"

#. i18n: ectx: property (text), widget (QLabel, u_gdbLabel)
#: advanced_settings.ui:17
#, kde-format
msgid "GDB command"
msgstr ""

#. i18n: ectx: property (text), widget (QToolButton, u_gdbBrowse)
#. i18n: ectx: property (text), widget (QToolButton, u_addSrcPath)
#. i18n: ectx: property (text), widget (QToolButton, u_delSrcPath)
#. i18n: ectx: property (text), widget (QToolButton, u_setSoPrefix)
#. i18n: ectx: property (text), widget (QToolButton, u_addSoSearchPath)
#. i18n: ectx: property (text), widget (QToolButton, u_delSoSearchPath)
#: advanced_settings.ui:30 advanced_settings.ui:62 advanced_settings.ui:69
#: advanced_settings.ui:241 advanced_settings.ui:274 advanced_settings.ui:281
#, kde-format
msgid "..."
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, u_srcPathsLabel)
#: advanced_settings.ui:37
#, kde-format
msgid "Source file search paths"
msgstr ""

#. i18n: ectx: property (text), item, widget (QComboBox, u_localRemote)
#: advanced_settings.ui:92
#, kde-format
msgid "Local application"
msgstr ""

#. i18n: ectx: property (text), item, widget (QComboBox, u_localRemote)
#: advanced_settings.ui:97
#, kde-format
msgid "Remote TCP"
msgstr ""

#. i18n: ectx: property (text), item, widget (QComboBox, u_localRemote)
#: advanced_settings.ui:102
#, fuzzy, kde-format
#| msgid "Remove breakpoint"
msgid "Remote Serial Port"
msgstr "Elimină punct de oprire"

#. i18n: ectx: property (text), widget (QLabel, u_hostLabel)
#: advanced_settings.ui:127
#, kde-format
msgid "Host"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, u_tcpPortLabel)
#. i18n: ectx: property (text), widget (QLabel, u_ttyLabel)
#: advanced_settings.ui:141 advanced_settings.ui:166
#, kde-format
msgid "Port"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, u_ttyBaudLabel)
#: advanced_settings.ui:183
#, kde-format
msgid "Baud"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, u_soAbsPrefixLabel)
#: advanced_settings.ui:231
#, kde-format
msgid "solib-absolute-prefix"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, u_soSearchLabel)
#: advanced_settings.ui:248
#, kde-format
msgid "solib-search-path"
msgstr ""

#. i18n: ectx: property (title), widget (QGroupBox, u_customInitGB)
#: advanced_settings.ui:317
#, kde-format
msgid "Custom Init Commands"
msgstr ""

#: backend.cpp:24 backend.cpp:49 debugview_dap.cpp:155
#, kde-format
msgid ""
"A debugging session is on course. Please, use re-run or stop the current "
"session."
msgstr ""

#: configview.cpp:91
#, fuzzy, kde-format
#| msgid "Add executable target"
msgid "Add new target"
msgstr "Adaugă țintă pentru executabil"

#: configview.cpp:95
#, fuzzy, kde-format
#| msgid "Remove target"
msgid "Copy target"
msgstr "Elimină ținta"

#: configview.cpp:99
#, fuzzy, kde-format
#| msgid "Remove target"
msgid "Delete target"
msgstr "Elimină ținta"

#: configview.cpp:104
#, kde-format
msgid "Executable:"
msgstr ""

#: configview.cpp:124
#, fuzzy, kde-format
#| msgid "&Working Directory:"
msgid "Working Directory:"
msgstr "&Dosar de lucru:"

#: configview.cpp:132
#, kde-format
msgid "Process Id:"
msgstr ""

#: configview.cpp:137
#, fuzzy, kde-format
#| msgid "Add Argument List"
msgctxt "Program argument list"
msgid "Arguments:"
msgstr "Adaugă listă de argumenți"

#: configview.cpp:140
#, kde-format
msgctxt "Checkbox to for keeping focus on the command line"
msgid "Keep focus"
msgstr "Păstrează focalizarea"

#: configview.cpp:141
#, kde-format
msgid "Keep the focus on the command line"
msgstr "Păstrează focalizarea pe linia de comandă"

#: configview.cpp:143
#, kde-format
msgid "Redirect IO"
msgstr "Redirectează IO"

#: configview.cpp:144
#, kde-format
msgid "Redirect the debugged programs IO to a separate tab"
msgstr "Redirectează IO-ul programelor depanate către o filă separată"

#: configview.cpp:146
#, fuzzy, kde-format
#| msgctxt "Tab label"
#| msgid "Settings"
msgid "Advanced Settings"
msgstr "Configurări"

#: configview.cpp:231
#, kde-format
msgid "Targets"
msgstr "Ținte"

#: configview.cpp:524 configview.cpp:537
#, fuzzy, kde-format
#| msgid "Targets"
msgid "Target %1"
msgstr "Ținte"

#: debugview.cpp:35
#, fuzzy, kde-format
msgid "Locals"
msgstr "Locale"

#: debugview.cpp:37
#, kde-format
msgid "CPU registers"
msgstr ""

#: debugview.cpp:160
#, kde-format
msgid "Please set the executable in the 'Settings' tab in the 'Debug' panel."
msgstr ""

#: debugview.cpp:169
#, kde-format
msgid ""
"No debugger selected. Please select one in the 'Settings' tab in the 'Debug' "
"panel."
msgstr ""

#: debugview.cpp:178
#, kde-format
msgid ""
"Debugger not found. Please make sure you have it installed in your system. "
"The selected debugger is '%1'"
msgstr ""

#: debugview.cpp:384
#, kde-format
msgid "Could not start debugger process"
msgstr "Nu s-a putut porni procesul de depanare"

#: debugview.cpp:442
#, kde-format
msgid "*** gdb exited normally ***"
msgstr "*** gdb a ieșit normal ***"

#: debugview.cpp:648
#, kde-format
msgid "all threads running"
msgstr ""

#: debugview.cpp:650
#, fuzzy, kde-format
#| msgid "Thread %1"
msgid "thread(s) running: %1"
msgstr "Fir de execuție %1"

#: debugview.cpp:655 debugview_dap.cpp:270
#, kde-format
msgid "stopped (%1)."
msgstr ""

#: debugview.cpp:659 debugview_dap.cpp:278
#, kde-format
msgid "Active thread: %1 (all threads stopped)."
msgstr ""

#: debugview.cpp:661 debugview_dap.cpp:280
#, kde-format
msgid "Active thread: %1."
msgstr ""

#: debugview.cpp:680
#, kde-format
msgid "Current frame: %1:%2"
msgstr ""

#: debugview.cpp:707
#, fuzzy, kde-format
#| msgid "Targets"
msgid "Host: %1. Target: %1"
msgstr "Ținte"

#: debugview.cpp:1377
#, kde-format
msgid ""
"gdb-mi: Could not parse last response: %1. Too many consecutive errors. "
"Shutting down."
msgstr ""

#: debugview.cpp:1379
#, kde-format
msgid "gdb-mi: Could not parse last response: %1"
msgstr ""

#: debugview_dap.cpp:169
#, kde-format
msgid "DAP backend failed"
msgstr ""

#: debugview_dap.cpp:211
#, kde-format
msgid "program terminated"
msgstr ""

#: debugview_dap.cpp:223
#, kde-format
msgid "requesting disconnection"
msgstr ""

#: debugview_dap.cpp:237
#, kde-format
msgid "requesting shutdown"
msgstr ""

#: debugview_dap.cpp:261
#, kde-format
msgid "DAP backend: %1"
msgstr ""

#: debugview_dap.cpp:285
#, fuzzy, kde-format
#| msgid "Breakpoint"
msgid "Breakpoint(s) reached:"
msgstr "Punct de oprire"

#: debugview_dap.cpp:307
#, kde-format
msgid "(continued) thread %1"
msgstr ""

#: debugview_dap.cpp:309
#, kde-format
msgid "all threads continued"
msgstr ""

#: debugview_dap.cpp:316
#, kde-format
msgid "(running)"
msgstr ""

#: debugview_dap.cpp:404
#, kde-format
msgid "*** connection with server closed ***"
msgstr ""

#: debugview_dap.cpp:411
#, kde-format
msgid "program exited with code %1"
msgstr ""

#: debugview_dap.cpp:425
#, kde-format
msgid "*** waiting for user actions ***"
msgstr ""

#: debugview_dap.cpp:430
#, kde-format
msgid "error on response: %1"
msgstr ""

#: debugview_dap.cpp:445
#, kde-format
msgid "important"
msgstr ""

#: debugview_dap.cpp:448
#, kde-format
msgid "telemetry"
msgstr ""

#: debugview_dap.cpp:467
#, kde-format
msgid "debugging process [%1] %2"
msgstr ""

#: debugview_dap.cpp:469
#, kde-format
msgid "debugging process %1"
msgstr ""

#: debugview_dap.cpp:472
#, kde-format
msgid "Start method: %1"
msgstr ""

#: debugview_dap.cpp:479
#, fuzzy, kde-format
#| msgid "Thread %1"
msgid "thread %1"
msgstr "Fir de execuție %1"

#: debugview_dap.cpp:633
#, fuzzy, kde-format
#| msgid "Breakpoint"
msgid "breakpoint set"
msgstr "Punct de oprire"

#: debugview_dap.cpp:641
#, fuzzy, kde-format
#| msgid "Breakpoint"
msgid "breakpoint cleared"
msgstr "Punct de oprire"

#: debugview_dap.cpp:700
#, fuzzy, kde-format
#| msgid "Breakpoint"
msgid "(%1) breakpoint"
msgstr "Punct de oprire"

#: debugview_dap.cpp:717
#, kde-format
msgid "<not evaluated>"
msgstr ""

#: debugview_dap.cpp:739
#, kde-format
msgid "server capabilities"
msgstr ""

#: debugview_dap.cpp:742
#, kde-format
msgid "supported"
msgstr ""

#: debugview_dap.cpp:742
#, kde-format
msgid "unsupported"
msgstr ""

#: debugview_dap.cpp:745
#, fuzzy, kde-format
#| msgid "Insert breakpoint"
msgid "conditional breakpoints"
msgstr "Inserează punct de oprire"

#: debugview_dap.cpp:746
#, fuzzy, kde-format
#| msgid "Insert breakpoint"
msgid "function breakpoints"
msgstr "Inserează punct de oprire"

#: debugview_dap.cpp:747
#, kde-format
msgid "hit conditional breakpoints"
msgstr ""

#: debugview_dap.cpp:748
#, kde-format
msgid "log points"
msgstr ""

#: debugview_dap.cpp:748
#, kde-format
msgid "modules request"
msgstr ""

#: debugview_dap.cpp:749
#, kde-format
msgid "goto targets request"
msgstr ""

#: debugview_dap.cpp:750
#, kde-format
msgid "terminate request"
msgstr ""

#: debugview_dap.cpp:751
#, kde-format
msgid "terminate debuggee"
msgstr ""

#: debugview_dap.cpp:958
#, kde-format
msgid "syntax error: expression not found"
msgstr ""

#: debugview_dap.cpp:976 debugview_dap.cpp:1011 debugview_dap.cpp:1049
#: debugview_dap.cpp:1083 debugview_dap.cpp:1119 debugview_dap.cpp:1155
#: debugview_dap.cpp:1191 debugview_dap.cpp:1291 debugview_dap.cpp:1353
#, kde-format
msgid "syntax error: %1"
msgstr ""

#: debugview_dap.cpp:984 debugview_dap.cpp:1019 debugview_dap.cpp:1298
#: debugview_dap.cpp:1361
#, kde-format
msgid "invalid line: %1"
msgstr ""

#: debugview_dap.cpp:991 debugview_dap.cpp:996 debugview_dap.cpp:1026
#: debugview_dap.cpp:1031 debugview_dap.cpp:1322 debugview_dap.cpp:1327
#: debugview_dap.cpp:1368 debugview_dap.cpp:1373
#, kde-format
msgid "file not specified: %1"
msgstr ""

#: debugview_dap.cpp:1061 debugview_dap.cpp:1095 debugview_dap.cpp:1131
#: debugview_dap.cpp:1167 debugview_dap.cpp:1203
#, kde-format
msgid "invalid thread id: %1"
msgstr ""

#: debugview_dap.cpp:1067 debugview_dap.cpp:1101 debugview_dap.cpp:1137
#: debugview_dap.cpp:1173 debugview_dap.cpp:1209
#, kde-format
msgid "thread id not specified: %1"
msgstr ""

#: debugview_dap.cpp:1220
#, kde-format
msgid "Available commands:"
msgstr ""

#: debugview_dap.cpp:1308
#, kde-format
msgid "conditional breakpoints are not supported by the server"
msgstr ""

#: debugview_dap.cpp:1316
#, kde-format
msgid "hit conditional breakpoints are not supported by the server"
msgstr ""

#: debugview_dap.cpp:1336
#, kde-format
msgid "line %1 already has a breakpoint"
msgstr ""

#: debugview_dap.cpp:1381
#, kde-format
msgid "breakpoint not found (%1:%2)"
msgstr ""

#: debugview_dap.cpp:1387
#, kde-format
msgid "Current thread: "
msgstr ""

#: debugview_dap.cpp:1392 debugview_dap.cpp:1399 debugview_dap.cpp:1423
#, kde-format
msgid "none"
msgstr ""

#: debugview_dap.cpp:1395
#, kde-format
msgid "Current frame: "
msgstr ""

#: debugview_dap.cpp:1402
#, kde-format
msgid "Session state: "
msgstr ""

#: debugview_dap.cpp:1405
#, kde-format
msgid "initializing"
msgstr ""

#: debugview_dap.cpp:1408
#, kde-format
msgid "running"
msgstr ""

#: debugview_dap.cpp:1411
#, kde-format
msgid "stopped"
msgstr ""

#: debugview_dap.cpp:1414
#, kde-format
msgid "terminated"
msgstr ""

#: debugview_dap.cpp:1417
#, kde-format
msgid "disconnected"
msgstr ""

#: debugview_dap.cpp:1420
#, kde-format
msgid "post mortem"
msgstr ""

#: debugview_dap.cpp:1476
#, kde-format
msgid "command not found"
msgstr ""

#: debugview_dap.cpp:1497
#, kde-format
msgid "missing thread id"
msgstr ""

#: debugview_dap.cpp:1605
#, kde-format
msgid "killing backend"
msgstr ""

#: debugview_dap.cpp:1663
#, kde-format
msgid "Current frame [%3]: %1:%2 (%4)"
msgstr ""

#: localsview.cpp:17
#, kde-format
msgid "Symbol"
msgstr "Simbol"

#: localsview.cpp:18
#, kde-format
msgid "Value"
msgstr "Valoare"

#: localsview.cpp:41
#, kde-format
msgid "type"
msgstr ""

#: localsview.cpp:50
#, kde-format
msgid "indexed items"
msgstr ""

#: localsview.cpp:53
#, kde-format
msgid "named items"
msgstr ""

#: plugin_kategdb.cpp:66
#, kde-format
msgid "Kate GDB"
msgstr ""

#: plugin_kategdb.cpp:70
#, kde-format
msgid "Debug View"
msgstr "Vizualizare depanare"

#: plugin_kategdb.cpp:70 plugin_kategdb.cpp:285
#, kde-format
msgid "Debug"
msgstr "Depanează"

#: plugin_kategdb.cpp:73 plugin_kategdb.cpp:76
#, fuzzy, kde-format
#| msgid "Call Stack"
msgid "Locals and Stack"
msgstr "Stiva de apeluri"

#: plugin_kategdb.cpp:121
#, kde-format
msgctxt "Column label (frame number)"
msgid "Nr"
msgstr "Nr"

#: plugin_kategdb.cpp:121
#, kde-format
msgctxt "Column label"
msgid "Frame"
msgstr "Cadru"

#: plugin_kategdb.cpp:153
#, kde-format
msgctxt "Tab label"
msgid "GDB Output"
msgstr "Ieșire GDB"

#: plugin_kategdb.cpp:154
#, kde-format
msgctxt "Tab label"
msgid "Settings"
msgstr "Configurări"

#: plugin_kategdb.cpp:196
#, kde-kuit-format
msgctxt "@info"
msgid ""
"<title>Could not open file:</title><nl/>%1<br/>Try adding a search path to "
"Advanced Settings -> Source file search paths"
msgstr ""

#: plugin_kategdb.cpp:221
#, kde-format
msgid "Start Debugging"
msgstr "Începe depanarea"

#: plugin_kategdb.cpp:227
#, kde-format
msgid "Kill / Stop Debugging"
msgstr "Omoară / Oprește depanarea"

#: plugin_kategdb.cpp:233
#, kde-format
msgid "Restart Debugging"
msgstr "Repornește depanarea"

#: plugin_kategdb.cpp:239
#, kde-format
msgid "Toggle Breakpoint / Break"
msgstr "Comută între Punct de oprire / Oprire"

#: plugin_kategdb.cpp:245
#, kde-format
msgid "Step In"
msgstr "Pășește în"

#: plugin_kategdb.cpp:251
#, kde-format
msgid "Step Over"
msgstr "Pășește peste"

#: plugin_kategdb.cpp:257
#, kde-format
msgid "Step Out"
msgstr "Pășește în afară"

#: plugin_kategdb.cpp:263 plugin_kategdb.cpp:294
#, kde-format
msgctxt "Move Program Counter (next execution)"
msgid "Move PC"
msgstr "Mută PC"

#: plugin_kategdb.cpp:268 plugin_kategdb.cpp:292
#, kde-format
msgid "Run To Cursor"
msgstr "Rulează până la cursor"

#: plugin_kategdb.cpp:274
#, kde-format
msgid "Continue"
msgstr "Continuă"

#: plugin_kategdb.cpp:280
#, kde-format
msgid "Print Value"
msgstr "Tipărește valoarea"

#: plugin_kategdb.cpp:289
#, kde-format
msgid "popup_breakpoint"
msgstr "popup_breakpoint"

#: plugin_kategdb.cpp:291
#, kde-format
msgid "popup_run_to_cursor"
msgstr "popup_run_to_cursor"

#: plugin_kategdb.cpp:373 plugin_kategdb.cpp:389
#, kde-format
msgid "Insert breakpoint"
msgstr "Inserează punct de oprire"

#: plugin_kategdb.cpp:387
#, kde-format
msgid "Remove breakpoint"
msgstr "Elimină punct de oprire"

#: plugin_kategdb.cpp:519
#, kde-format
msgid "Execution point"
msgstr "Punct de execuție"

#: plugin_kategdb.cpp:662
#, kde-format
msgid "Thread %1"
msgstr "Fir de execuție %1"

#: plugin_kategdb.cpp:762
#, kde-format
msgid "IO"
msgstr "IO"

#: plugin_kategdb.cpp:848
#, kde-format
msgid "Breakpoint"
msgstr "Punct de oprire"

#. i18n: ectx: Menu (debug)
#: ui.rc:6
#, kde-format
msgid "&Debug"
msgstr "&Depanează"

#. i18n: ectx: ToolBar (gdbplugin)
#: ui.rc:29
#, kde-format
msgid "GDB Plugin"
msgstr "Modul GDB"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Sergiu Bivol, Cristian Buzdugă"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "sergiu@cip.md, cristianbzdg@gmail.com"

#~ msgid "GDB Integration"
#~ msgstr "Integrare GDB"

#~ msgid "Kate GDB Integration"
#~ msgstr "Integrare GDB Kate"

#~ msgid "&Target:"
#~ msgstr "Țin&tă:"

#~ msgctxt "Program argument list"
#~ msgid "&Arg List:"
#~ msgstr "Listă &argumenți:"

#~ msgid "Remove Argument List"
#~ msgstr "Elimină listă de argumenți"

#~ msgid "Arg Lists"
#~ msgstr "Liste arg"
