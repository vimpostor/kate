# Uyghur translation for kateproject.
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Gheyret Kenji <gheyret@gmail.com>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: kateproject\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-03-26 00:51+0000\n"
"PO-Revision-Date: 2013-09-08 07:04+0900\n"
"Last-Translator: Gheyret Kenji <gheyret@gmail.com>\n"
"Language-Team: Uyghur <kde-i18n-doc@kde.org>\n"
"Language: ug\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#: branchcheckoutdialog.cpp:29
#, kde-format
msgid "Select branch to checkout. Press 'Esc' to cancel."
msgstr ""

#: branchcheckoutdialog.cpp:36
#, kde-format
msgid "Create New Branch"
msgstr ""

#: branchcheckoutdialog.cpp:38
#, kde-format
msgid "Create New Branch From..."
msgstr ""

#: branchcheckoutdialog.cpp:53
#, kde-format
msgid "Branch %1 checked out"
msgstr ""

#: branchcheckoutdialog.cpp:56
#, kde-format
msgid "Failed to checkout to branch %1, Error: %2"
msgstr ""

#: branchcheckoutdialog.cpp:83 branchcheckoutdialog.cpp:95
#, kde-format
msgid "Enter new branch name. Press 'Esc' to cancel."
msgstr ""

#: branchcheckoutdialog.cpp:100
#, kde-format
msgid "Select branch to checkout from. Press 'Esc' to cancel."
msgstr ""

#: branchcheckoutdialog.cpp:121
#, kde-format
msgid "Checked out to new branch: %1"
msgstr ""

#: branchcheckoutdialog.cpp:123
#, kde-format
msgid "Failed to create new branch. Error \"%1\""
msgstr ""

#: branchdeletedialog.cpp:122
#, kde-format
msgid "Branch"
msgstr ""

#: branchdeletedialog.cpp:122
#, kde-format
msgid "Last Commit"
msgstr ""

#: branchdeletedialog.cpp:135 kateprojecttreeviewcontextmenu.cpp:78
#, kde-format
msgid "Delete"
msgstr ""

#: branchdeletedialog.cpp:140
#, kde-format
msgid "Are you sure you want to delete the selected branch?"
msgid_plural "Are you sure you want to delete the selected branches?"
msgstr[0] ""

#: branchesdialog.cpp:102
#, kde-format
msgid "Select Branch..."
msgstr ""

#: branchesdialog.cpp:128 gitwidget.cpp:490 kateprojectpluginview.cpp:68
#, kde-format
msgid "Git"
msgstr ""

#: comparebranchesview.cpp:144
#, kde-format
msgid "Back"
msgstr ""

#: currentgitbranchbutton.cpp:123
#, kde-format
msgctxt "Tooltip text, describing that '%1' commit is checked out"
msgid "HEAD at commit %1"
msgstr ""

#: currentgitbranchbutton.cpp:125
#, kde-format
msgctxt "Tooltip text, describing that '%1' tag is checked out"
msgid "HEAD is at this tag %1"
msgstr ""

#: currentgitbranchbutton.cpp:127
#, kde-format
msgctxt "Tooltip text, describing that '%1' branch is checked out"
msgid "Active branch: %1"
msgstr ""

#: gitcommitdialog.cpp:69 gitcommitdialog.cpp:116
#, kde-format
msgid "Commit Changes"
msgstr ""

#: gitcommitdialog.cpp:73 gitcommitdialog.cpp:115 gitwidget.cpp:251
#, kde-format
msgid "Commit"
msgstr ""

#: gitcommitdialog.cpp:74
#, kde-format
msgid "Cancel"
msgstr ""

#: gitcommitdialog.cpp:76
#, kde-format
msgid "Write commit message..."
msgstr ""

#: gitcommitdialog.cpp:83
#, kde-format
msgid "Extended commit description..."
msgstr ""

#: gitcommitdialog.cpp:107
#, kde-format
msgid "Sign off"
msgstr ""

#: gitcommitdialog.cpp:111 gitcommitdialog.cpp:120
#, kde-format
msgid "Amend"
msgstr ""

#: gitcommitdialog.cpp:112 gitwidget.cpp:961
#, kde-format
msgid "Amend Last Commit"
msgstr ""

#: gitcommitdialog.cpp:119
#, kde-format
msgid "Amending Commit"
msgstr ""

#: gitcommitdialog.cpp:203
#, kde-format
msgctxt "Number of characters"
msgid "%1 / 52"
msgstr ""

#: gitcommitdialog.cpp:207
#, kde-format
msgctxt "Number of characters"
msgid "<span style=\"color:%1;\">%2</span> / 52"
msgstr ""

#: gitstatusmodel.cpp:85
#, kde-format
msgid "Staged"
msgstr ""

#: gitstatusmodel.cpp:87
#, fuzzy, kde-format
#| msgid "<untracked>"
msgid "Untracked"
msgstr "<ئىزلانمىغان>"

#: gitstatusmodel.cpp:89
#, kde-format
msgid "Conflict"
msgstr ""

#: gitstatusmodel.cpp:91
#, kde-format
msgid "Modified"
msgstr ""

#: gitwidget.cpp:259
#, kde-format
msgid "Git Push"
msgstr ""

#: gitwidget.cpp:272
#, kde-format
msgid "Git Pull"
msgstr ""

#: gitwidget.cpp:285
#, kde-format
msgid "Cancel Operation"
msgstr ""

#: gitwidget.cpp:293
#, kde-format
msgid " canceled."
msgstr ""

#: gitwidget.cpp:314 kateprojectview.cpp:59
#, kde-format
msgid "Filter..."
msgstr ""

#: gitwidget.cpp:401
#, kde-format
msgid "Failed to find .git directory for '%1', things may not work correctly"
msgstr ""

#: gitwidget.cpp:575
#, kde-format
msgid " error: %1"
msgstr ""

#: gitwidget.cpp:581
#, kde-format
msgid "\"%1\" executed successfully: %2"
msgstr ""

#: gitwidget.cpp:601
#, kde-format
msgid "Failed to stage file. Error:"
msgstr ""

#: gitwidget.cpp:614
#, kde-format
msgid "Failed to unstage file. Error:"
msgstr ""

#: gitwidget.cpp:625
#, kde-format
msgid "Failed to discard changes. Error:"
msgstr ""

#: gitwidget.cpp:636
#, kde-format
msgid "Failed to remove. Error:"
msgstr ""

#: gitwidget.cpp:652
#, kde-format
msgid "Failed to open file at HEAD: %1"
msgstr ""

#: gitwidget.cpp:684
#, kde-format
msgid "Failed to get Diff of file: %1"
msgstr ""

#: gitwidget.cpp:751
#, kde-format
msgid "Failed to commit: %1"
msgstr ""

#: gitwidget.cpp:755
#, kde-format
msgid "Changes committed successfully."
msgstr ""

#: gitwidget.cpp:765
#, kde-format
msgid "Nothing to commit. Please stage your changes first."
msgstr ""

#: gitwidget.cpp:778
#, kde-format
msgid "Commit message cannot be empty."
msgstr ""

#: gitwidget.cpp:893
#, kde-format
msgid "No diff for %1...%2"
msgstr ""

#: gitwidget.cpp:899
#, kde-format
msgid "Failed to compare %1...%2"
msgstr ""

#: gitwidget.cpp:914
#, kde-format
msgid "Failed to get numstat when diffing %1...%2"
msgstr ""

#: gitwidget.cpp:953
#, kde-format
msgid "Refresh"
msgstr ""

#: gitwidget.cpp:969
#, kde-format
msgid "Checkout Branch"
msgstr ""

#: gitwidget.cpp:981
#, kde-format
msgid "Delete Branch"
msgstr ""

#: gitwidget.cpp:993
#, kde-format
msgid "Compare Branch with..."
msgstr ""

#: gitwidget.cpp:997
#, kde-format
msgid "Show Commit"
msgstr ""

#: gitwidget.cpp:997
#, kde-format
msgid "Commit hash"
msgstr ""

#: gitwidget.cpp:1002
#, fuzzy, kde-format
#| msgid "Open With"
msgid "Open Commit..."
msgstr "بۇنىڭدا ئېچىش"

#: gitwidget.cpp:1005 gitwidget.cpp:1044
#, kde-format
msgid "Stash"
msgstr ""

#: gitwidget.cpp:1015
#, kde-format
msgid "Diff - stash"
msgstr ""

#: gitwidget.cpp:1048
#, kde-format
msgid "Pop Last Stash"
msgstr ""

#: gitwidget.cpp:1052
#, kde-format
msgid "Pop Stash"
msgstr ""

#: gitwidget.cpp:1056
#, kde-format
msgid "Apply Last Stash"
msgstr ""

#: gitwidget.cpp:1058
#, kde-format
msgid "Stash (Keep Staged)"
msgstr ""

#: gitwidget.cpp:1062
#, kde-format
msgid "Stash (Include Untracked)"
msgstr ""

#: gitwidget.cpp:1066
#, kde-format
msgid "Apply Stash"
msgstr ""

#: gitwidget.cpp:1067
#, kde-format
msgid "Drop Stash"
msgstr ""

#: gitwidget.cpp:1068
#, kde-format
msgid "Show Stash Content"
msgstr ""

#: gitwidget.cpp:1108
#, kde-format
msgid "Stage All"
msgstr ""

#: gitwidget.cpp:1110
#, kde-format
msgid "Remove All"
msgstr ""

#: gitwidget.cpp:1110
#, kde-format
msgid "Discard All"
msgstr ""

#: gitwidget.cpp:1113
#, kde-format
msgid "Open .gitignore"
msgstr ""

#: gitwidget.cpp:1114 gitwidget.cpp:1169 gitwidget.cpp:1219
#: kateprojectconfigpage.cpp:91 kateprojectconfigpage.cpp:102
#, kde-format
msgid "Show Diff"
msgstr ""

#: gitwidget.cpp:1131
#, kde-format
msgid "Are you sure you want to remove these files?"
msgstr ""

#: gitwidget.cpp:1140
#, kde-format
msgid "Are you sure you want to discard all changes?"
msgstr ""

#: gitwidget.cpp:1168
#, fuzzy, kde-format
#| msgid "Open With"
msgid "Open File"
msgstr "بۇنىڭدا ئېچىش"

#: gitwidget.cpp:1170
#, kde-format
msgid "Show in External Git Diff Tool"
msgstr ""

#: gitwidget.cpp:1171
#, kde-format
msgid "Open at HEAD"
msgstr ""

#: gitwidget.cpp:1172
#, kde-format
msgid "Unstage File"
msgstr ""

#: gitwidget.cpp:1172
#, kde-format
msgid "Stage File"
msgstr ""

#: gitwidget.cpp:1173
#, kde-format
msgid "Remove"
msgstr ""

#: gitwidget.cpp:1173
#, kde-format
msgid "Discard"
msgstr ""

#: gitwidget.cpp:1190
#, kde-format
msgid "Are you sure you want to discard the changes in this file?"
msgstr ""

#: gitwidget.cpp:1203
#, kde-format
msgid "Are you sure you want to remove this file?"
msgstr ""

#: gitwidget.cpp:1218
#, kde-format
msgid "Unstage All"
msgstr ""

#: gitwidget.cpp:1285
#, kde-format
msgid "Unstage Selected Files"
msgstr ""

#: gitwidget.cpp:1285
#, kde-format
msgid "Stage Selected Files"
msgstr ""

#: gitwidget.cpp:1286
#, kde-format
msgid "Discard Selected Files"
msgstr ""

#: gitwidget.cpp:1290
#, kde-format
msgid "Remove Selected Files"
msgstr ""

#: gitwidget.cpp:1303
#, kde-format
msgid "Are you sure you want to discard the changes?"
msgstr ""

#: gitwidget.cpp:1312
#, kde-format
msgid "Are you sure you want to remove these untracked changes?"
msgstr ""

#: kateproject.cpp:217
#, kde-format
msgid "Malformed JSON file '%1': %2"
msgstr ""

#: kateproject.cpp:529
#, kde-format
msgid "<untracked>"
msgstr "<ئىزلانمىغان>"

#: kateprojectcompletion.cpp:43
#, kde-format
msgid "Project Completion"
msgstr "قۇرۇلۇشنىڭ تاماملىنىشى"

#: kateprojectconfigpage.cpp:25
#, kde-format
msgctxt "Groupbox title"
msgid "Autoload Repositories"
msgstr ""

#: kateprojectconfigpage.cpp:27
#, kde-format
msgid ""
"Project plugin is able to autoload repository working copies when there is "
"no .kateproject file defined yet."
msgstr ""

#: kateprojectconfigpage.cpp:30
#, kde-format
msgid "&Git"
msgstr ""

#: kateprojectconfigpage.cpp:33
#, kde-format
msgid "&Subversion"
msgstr ""

#: kateprojectconfigpage.cpp:35
#, kde-format
msgid "&Mercurial"
msgstr ""

#: kateprojectconfigpage.cpp:37
#, kde-format
msgid "&Fossil"
msgstr ""

#: kateprojectconfigpage.cpp:46
#, kde-format
msgctxt "Groupbox title"
msgid "Session Behavior"
msgstr ""

#: kateprojectconfigpage.cpp:47
#, kde-format
msgid "Session settings for projects"
msgstr ""

#: kateprojectconfigpage.cpp:48
#, fuzzy, kde-format
#| msgid "Current Project"
msgid "Restore Open Projects"
msgstr "نۆۋەتتىكى قۇرۇلۇش"

#: kateprojectconfigpage.cpp:55
#, fuzzy, kde-format
#| msgid "Projects"
msgctxt "Groupbox title"
msgid "Project Index"
msgstr "قۇرۇلۇشلار"

#: kateprojectconfigpage.cpp:56
#, kde-format
msgid "Project ctags index settings"
msgstr ""

#: kateprojectconfigpage.cpp:57 kateprojectinfoviewindex.cpp:206
#, kde-format
msgid "Enable indexing"
msgstr ""

#: kateprojectconfigpage.cpp:60
#, kde-format
msgid "Directory for index files"
msgstr ""

#: kateprojectconfigpage.cpp:64
#, kde-format
msgid ""
"The system temporary directory is used if not specified, which may overflow "
"for very large repositories"
msgstr ""

#: kateprojectconfigpage.cpp:71
#, kde-format
msgctxt "Groupbox title"
msgid "Cross-Project Functionality"
msgstr ""

#: kateprojectconfigpage.cpp:72
#, kde-format
msgid ""
"Project plugin is able to perform some operations across multiple projects"
msgstr ""

#: kateprojectconfigpage.cpp:73
#, fuzzy, kde-format
#| msgid "Project Completion"
msgid "Cross-Project Completion"
msgstr "قۇرۇلۇشنىڭ تاماملىنىشى"

#: kateprojectconfigpage.cpp:75
#, kde-format
msgid "Cross-Project Goto Symbol"
msgstr ""

#: kateprojectconfigpage.cpp:83
#, kde-format
msgctxt "Groupbox title"
msgid "Git"
msgstr ""

#: kateprojectconfigpage.cpp:84
#, kde-format
msgid "Show number of changed lines in git status"
msgstr ""

#: kateprojectconfigpage.cpp:88
#, kde-format
msgid "Single click action in the git status view"
msgstr ""

#: kateprojectconfigpage.cpp:90 kateprojectconfigpage.cpp:101
#, kde-format
msgid "No Action"
msgstr ""

#: kateprojectconfigpage.cpp:92 kateprojectconfigpage.cpp:103
#, fuzzy, kde-format
#| msgid "Open With"
msgid "Open file"
msgstr "بۇنىڭدا ئېچىش"

#: kateprojectconfigpage.cpp:93 kateprojectconfigpage.cpp:104
#, kde-format
msgid "Stage / Unstage"
msgstr ""

#: kateprojectconfigpage.cpp:99
#, kde-format
msgid "Double click action in the git status view"
msgstr ""

#: kateprojectconfigpage.cpp:135 kateprojectpluginview.cpp:67
#, kde-format
msgid "Projects"
msgstr "قۇرۇلۇشلار"

#: kateprojectconfigpage.cpp:140
#, fuzzy, kde-format
#| msgid "Project Completion"
msgctxt "Groupbox title"
msgid "Projects Properties"
msgstr "قۇرۇلۇشنىڭ تاماملىنىشى"

#: kateprojectinfoview.cpp:35
#, kde-format
msgid "Terminal (.kateproject)"
msgstr ""

#: kateprojectinfoview.cpp:43
#, fuzzy, kde-format
#| msgid "Terminal"
msgid "Terminal (Base)"
msgstr "تېرمىنال"

#: kateprojectinfoview.cpp:50
#, kde-format
msgid "Code Index"
msgstr "كود ئىدېكىسى"

#: kateprojectinfoview.cpp:55
#, kde-format
msgid "Code Analysis"
msgstr "كود ئانالىزى"

#: kateprojectinfoview.cpp:60
#, kde-format
msgid "Notes"
msgstr "ئىزاھ"

#: kateprojectinfoviewcodeanalysis.cpp:34
#, kde-format
msgid "Start Analysis..."
msgstr "ئانالىزنى باشلا…"

#: kateprojectinfoviewcodeanalysis.cpp:98
#, kde-format
msgid ""
"%1<br/><br/>The tool will be run on all project files which match this list "
"of file extensions:<br/><br/><b>%2</b>"
msgstr ""

#: kateprojectinfoviewcodeanalysis.cpp:136
#: kateprojectinfoviewcodeanalysis.cpp:196
#: kateprojectinfoviewcodeanalysis.cpp:200
#, fuzzy, kde-format
#| msgid "Code Analysis"
msgid "CodeAnalysis"
msgstr "كود ئانالىزى"

#: kateprojectinfoviewcodeanalysis.cpp:191
#, kde-format
msgctxt ""
"Message to the user that analysis finished. %1 is the name of the program "
"that did the analysis, %2 is a number. e.g., [clang-tidy]Analysis on 5 files "
"finished"
msgid "[%1]Analysis on %2 file finished."
msgid_plural "[%1]Analysis on %2 files finished."
msgstr[0] ""

#: kateprojectinfoviewcodeanalysis.cpp:199
#, kde-format
msgid "Analysis failed with exit code %1, Error: %2"
msgstr ""

#: kateprojectinfoviewindex.cpp:33
#, kde-format
msgid "Name"
msgstr "ئاتى"

#: kateprojectinfoviewindex.cpp:33
#, kde-format
msgid "Kind"
msgstr "تۈر"

#: kateprojectinfoviewindex.cpp:33
#, kde-format
msgid "File"
msgstr "ھۆججەت"

#: kateprojectinfoviewindex.cpp:33
#, kde-format
msgid "Line"
msgstr "قۇر"

#: kateprojectinfoviewindex.cpp:34
#, kde-format
msgid "Search"
msgstr ""

#: kateprojectinfoviewindex.cpp:195
#, kde-format
msgid "The index could not be created. Please install 'ctags'."
msgstr "ئىندېكىسنى قۇرغىلى بولمىدى. ‹ctags› نى ئورنىتىڭ."

#: kateprojectinfoviewindex.cpp:205
#, kde-format
msgid "Indexing is not enabled"
msgstr ""

#: kateprojectitem.cpp:166
#, kde-format
msgid "Error"
msgstr ""

#: kateprojectitem.cpp:166
#, kde-format
msgid "File name already exists"
msgstr ""

#: kateprojectplugin.cpp:217
#, kde-format
msgid "Confirm project closing: %1"
msgstr ""

#: kateprojectplugin.cpp:218
#, kde-format
msgid "Do you want to close the project %1 and the related %2 open documents?"
msgstr ""

#: kateprojectplugin.cpp:570
#, kde-format
msgid "Full path to current project excluding the file name."
msgstr ""

#: kateprojectplugin.cpp:587
#, kde-format
msgid ""
"Full path to current project excluding the file name, with native path "
"separator (backslash on Windows)."
msgstr ""

#: kateprojectplugin.cpp:704 kateprojectpluginview.cpp:73
#: kateprojectpluginview.cpp:225 kateprojectviewtree.cpp:135
#: kateprojectviewtree.cpp:156
#, fuzzy, kde-format
#| msgid "Projects"
msgid "Project"
msgstr "قۇرۇلۇشلار"

#: kateprojectpluginview.cpp:57
#, fuzzy, kde-format
#| msgid "Projects"
msgid "Project Manager"
msgstr "قۇرۇلۇشلار"

#: kateprojectpluginview.cpp:79
#, fuzzy, kde-format
#| msgid "Current Project"
msgid "Open projects list"
msgstr "نۆۋەتتىكى قۇرۇلۇش"

#: kateprojectpluginview.cpp:84
#, fuzzy, kde-format
#| msgid "Current Project"
msgid "Reload project"
msgstr "نۆۋەتتىكى قۇرۇلۇش"

#: kateprojectpluginview.cpp:87
#, fuzzy, kde-format
#| msgid "Current Project"
msgid "Close project"
msgstr "نۆۋەتتىكى قۇرۇلۇش"

#: kateprojectpluginview.cpp:102
#, kde-format
msgid "Refresh git status"
msgstr ""

#: kateprojectpluginview.cpp:180
#, fuzzy, kde-format
#| msgid "Open With"
msgid "Open Folder..."
msgstr "بۇنىڭدا ئېچىش"

#: kateprojectpluginview.cpp:185
#, fuzzy, kde-format
#| msgid "Projects"
msgid "Project TODOs"
msgstr "قۇرۇلۇشلار"

#: kateprojectpluginview.cpp:189
#, kde-format
msgid "Activate Previous Project"
msgstr ""

#: kateprojectpluginview.cpp:194
#, kde-format
msgid "Activate Next Project"
msgstr ""

#: kateprojectpluginview.cpp:199
#, kde-format
msgid "Lookup"
msgstr ""

#: kateprojectpluginview.cpp:203
#, fuzzy, kde-format
#| msgid "Current Project"
msgid "Close Project"
msgstr "نۆۋەتتىكى قۇرۇلۇش"

#: kateprojectpluginview.cpp:207
#, fuzzy, kde-format
#| msgid "Current Project"
msgid "Close All Projects"
msgstr "نۆۋەتتىكى قۇرۇلۇش"

#: kateprojectpluginview.cpp:212
#, fuzzy, kde-format
#| msgid "Current Project"
msgid "Close Orphaned Projects"
msgstr "نۆۋەتتىكى قۇرۇلۇش"

#: kateprojectpluginview.cpp:222
#, kde-format
msgid "Checkout Git Branch"
msgstr ""

#: kateprojectpluginview.cpp:228 kateprojectpluginview.cpp:798
#, kde-format
msgid "Lookup: %1"
msgstr ""

#: kateprojectpluginview.cpp:229 kateprojectpluginview.cpp:799
#, kde-format
msgid "Goto: %1"
msgstr ""

#: kateprojectpluginview.cpp:303
#, fuzzy, kde-format
#| msgid "Projects"
msgid "Projects Index"
msgstr "قۇرۇلۇشلار"

#: kateprojectpluginview.cpp:843
#, kde-format
msgid "Choose a directory"
msgstr ""

#: kateprojecttreeviewcontextmenu.cpp:42
#, kde-format
msgid "Enter name:"
msgstr ""

#: kateprojecttreeviewcontextmenu.cpp:43
#, kde-format
msgid "Add"
msgstr ""

#: kateprojecttreeviewcontextmenu.cpp:64
#, kde-format
msgid "Copy Location"
msgstr ""

#: kateprojecttreeviewcontextmenu.cpp:69
#, fuzzy, kde-format
#| msgid "File"
msgid "Add File"
msgstr "ھۆججەت"

#: kateprojecttreeviewcontextmenu.cpp:70
#, kde-format
msgid "Add Folder"
msgstr ""

#: kateprojecttreeviewcontextmenu.cpp:77
#, kde-format
msgid "&Rename"
msgstr ""

#: kateprojecttreeviewcontextmenu.cpp:84
#, fuzzy, kde-format
#| msgid "Project Completion"
msgid "Properties"
msgstr "قۇرۇلۇشنىڭ تاماملىنىشى"

#: kateprojecttreeviewcontextmenu.cpp:88
#, kde-format
msgid "Open With"
msgstr "بۇنىڭدا ئېچىش"

#: kateprojecttreeviewcontextmenu.cpp:96
#, kde-format
msgid "Open Internal Terminal Here"
msgstr ""

#: kateprojecttreeviewcontextmenu.cpp:105
#, kde-format
msgid "Open External Terminal Here"
msgstr ""

#: kateprojecttreeviewcontextmenu.cpp:110
#, kde-format
msgid "&Open Containing Folder"
msgstr ""

#: kateprojecttreeviewcontextmenu.cpp:120
#, kde-format
msgid "Show Git History"
msgstr ""

#: kateprojecttreeviewcontextmenu.cpp:130
#, kde-format
msgid "Delete File"
msgstr ""

#: kateprojecttreeviewcontextmenu.cpp:131
#, kde-format
msgid "Do you want to delete the file '%1'?"
msgstr ""

#: kateprojectviewtree.cpp:135
#, kde-format
msgid "Failed to create file: %1, Error: %2"
msgstr ""

#: kateprojectviewtree.cpp:156
#, kde-format
msgid "Failed to create dir: %1"
msgstr ""

#: stashdialog.cpp:37
#, kde-format
msgid "Stash message (optional). Enter to confirm, Esc to leave."
msgstr ""

#: stashdialog.cpp:44
#, kde-format
msgid "Type to filter, Enter to pop stash, Esc to leave."
msgstr ""

#: stashdialog.cpp:143
#, kde-format
msgid "Failed to stash changes %1"
msgstr ""

#: stashdialog.cpp:145
#, kde-format
msgid "Changes stashed successfully."
msgstr ""

#: stashdialog.cpp:164
#, kde-format
msgid "Failed to get stash list. Error: "
msgstr ""

#: stashdialog.cpp:180
#, kde-format
msgid "Failed to apply stash. Error: "
msgstr ""

#: stashdialog.cpp:182
#, kde-format
msgid "Failed to drop stash. Error: "
msgstr ""

#: stashdialog.cpp:184
#, kde-format
msgid "Failed to pop stash. Error: "
msgstr ""

#: stashdialog.cpp:188
#, kde-format
msgid "Stash applied successfully."
msgstr ""

#: stashdialog.cpp:190
#, kde-format
msgid "Stash dropped successfully."
msgstr ""

#: stashdialog.cpp:192
#, kde-format
msgid "Stash popped successfully."
msgstr ""

#: stashdialog.cpp:221
#, kde-format
msgid "Show stash failed. Error: "
msgstr ""

#. i18n: ectx: Menu (projects)
#: ui.rc:9
#, kde-format
msgid "&Projects"
msgstr "قۇرۇلۇشلار(&P)"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "ئابدۇقادىر ئابلىز, غەيرەت كەنجى"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "sahran.ug@gmail.com,  gheyret@gmail.com"

#, fuzzy
#~| msgid "Copy Filename"
#~ msgid "Copy File Path"
#~ msgstr "كۆچۈرىدىغان ھۆججەت ئاتى"

#, fuzzy
#~| msgid "Open With"
#~ msgid "Type to filter..."
#~ msgstr "بۇنىڭدا ئېچىش"

#, fuzzy
#~| msgid "<untracked>"
#~ msgid "Untracked (%1)"
#~ msgstr "<ئىزلانمىغان>"

#~ msgid "Please install 'cppcheck'."
#~ msgstr "‹cppcheck› نى ئورنىتىڭ."

#~ msgid "Git Tools"
#~ msgstr "Git قورالى"

#~ msgid "Launch gitk"
#~ msgstr "gitk نى ئىجرا قىل"

#~ msgid "Launch qgit"
#~ msgstr "qgit نى ئىجرا قىل"

#~ msgid "Launch git-cola"
#~ msgstr "git-cola نى ئىجرا قىل"

#~ msgid "Hello World"
#~ msgstr "سالام دۇنيا"

#~ msgid "Example kate plugin"
#~ msgstr "kate مىسال قىستۇرمىسى"
